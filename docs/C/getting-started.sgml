<!-- -*- sgml -*- -->
<!-- CH: GETTING STARTED -->
                                                                                
<chapter>
  <title>Getting Started</title>
                                                                                
  <abstract>
    <simpara>
	  This chapter gets you going with <application>Granule</application>.
    </simpara>
  </abstract>

  <!-- REQUIREMENTS -->
  <sect1>
	<title>Application Requirements And Installation</title>

	<para><application>Granule</application> is distributed both in tarball
	  (source code) and RPM package formats. You also have to satisfy several
	  library dependencies listed on the project's 
	  <ulink url="http://granule.sourceforge.net/grapp_download.html">Download
	  </ulink> page.
	</para>

	<para>To install RPM package, download the latest version from 
	  <ulink url="https://sourceforge.net/project/showfiles.php?group_id=111131">SourceForge download page</ulink> 
	  (click on [<emphasis>View All Project Files</emphasis>]), and then</para>
	
	<screen>
	  <prompt>$</prompt> <command>su</command>
	  <prompt>Password:</prompt> <command>YOUR-PASSWORD-HERE</command>
	  <prompt>#</prompt> <command>rpm -Uhv granule-VERSION.i386.rpm</command>
	  <prompt>#</prompt> <command>exit</command>
	  <prompt>$</prompt>
	</screen>
	
	<para>If you want to build <application>Granule</application> from 
	  scratch, first, install all dependency libraries, download latest 
	  <emphasis role="strong">granule-VERSION.tar.gz</emphasis> from 
	  <ulink url="https://sourceforge.net/project/showfiles.php?group_id=111131">SourceForge download page</ulink>, and then</para>
	
	<screen>
	  <prompt>$</prompt> <command>tar xvfz granule-VERSION.tar.gz</command>
	  <prompt>$</prompt> <command>cd granule-VERSION</command>
	  <prompt>$</prompt> <command>./configure</command>
	  <prompt>$</prompt> <command>make</command>
	  <prompt>$</prompt> <command>su</command>
	  <prompt>Password:</prompt> <command>YOUR-PASSWORD-HERE</command>
	  <prompt>#</prompt> <command>make install</command>
	  <prompt>#</prompt> <command>exit</command>
	  <prompt>$</prompt>
	</screen>
	
  </sect1>

  <!-- TO START GRANULE -->
  <sect1>
	<title>To Start Granule</title>

	<para>You can start <application>Granule</application> in the following 
	  ways:
	</para>
	
	<variablelist>
	  <varlistentry>
		<term><emphasis role="strong">Applications</emphasis> Menu</term>
		<listitem>
		  <para>Choose <menuchoice><guimenu>Accessories</guimenu>
			  <guisubmenu>Granule</guisubmenu></menuchoice>.
		  </para>
		</listitem>
	  </varlistentry>
	  <varlistentry>
		<term>Command Line</term>
		<listitem>
		  <para>Type <application>granule</application>, and then press
			<keycap>Enter</keycap>.
		  </para>
		</listitem>
	  </varlistentry>
	</variablelist>

	<!-- WHEN YOU START GRANULE -->
	<sect2>
	  <title>When You Start Granule</title>

	  <para>When you start <application>Granule</application>, the following
		window is displayed.
	  </para>
	  
	  <figure id="fig-01">
		<title>Main Window Dialog</title>
		<mediaobject>
<![%printout;[
		  <imageobject>
			<imagedata fileref="figures/grapp-main-win.png" format="png" scale="50">
		  </imageobject>
]]>
<![%online;[
		  <imageobject>
			<imagedata fileref="../figures/grapp-main-win.png" format="png">
		  </imageobject>
]]>
		  <textobject>
			<para>MainWindow Dialog</para>
		  </textobject>
		</mediaobject>
	  </figure>

	  <para>The <emphasis role="strong">Main Window</emphasis> dialog consists
		of the following elements:</para>

	  <variablelist>
	    <varlistentry>
		  <term>Menubar</term>
		  <listitem>
			<para>Contains standard menus.</para>
		  </listitem>
		</varlistentry>
		<varlistentry>
		  <term>CardBox pane</term>
		  <listitem>
			<para>Holds five card boxes (or partitions).</para>
		  </listitem>
		</varlistentry>
		<varlistentry>
		  <term>Decks pane</term>
		  <listitem>
			<para>Contains the names of all opened card decks - 
			  a <emphasis>DeckList</emphasis>. Once a Deck is opened,
			  it stays open until you exit from 
			  <application>Granule</application>.
			</para>
		  </listitem>
		</varlistentry>
	  </variablelist>
	</sect2>
  </sect1>

  <!-- --------------------------------- -->
  <sect1>
	<title>To Exit Granule</title>

	<para>To exit <application>Granule</application>, choose
	  <menuchoice><guimenu>File</guimenu>
		<guisubmenu>Exit</guisubmenu></menuchoice> menu item.
	</para>

	<figure id="fig-11">
	  <title>Exit Granule</title>
	  <mediaobject>
<![%printout;[
		<imageobject>
		  <imagedata fileref="figures/grapp-exit.png" format="png" scale="50">
		</imageobject>
]]>
<![%online;[
		<imageobject>
		  <imagedata fileref="../figures/grapp-exit.png" 
			format="png">
		</imageobject>
]]>
		<textobject>
		  <para>grapp-exit.png</para>
		</textobject>
	  </mediaobject>
	</figure>
  </sect1>

</chapter>

<!--=20
  Local Variables:
  mode: sgml
  sgml-declaration: "chapter.decl"
  sgml-indent-data: t
  sgml-omittag: nil
  sgml-always-quote-attributes: t
  sgml-parent-document: ("granule-manual.sgml" "book" "chapter")
  End:
-->
