%define name            granule-manual
%define ver             1.1.0
%define RELEASE         0
%define rel             %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define prefix          /usr
%define mandir          /usr/man
%define datadir         /usr/share
%define sysconfdir      /etc
%define localstatedir   /var/lib


Summary:    granule User's Manual
Name:       %name
Version:    %ver
Release:    1
Source:     %{name}-%{version}.tar.gz
Copyright:  GNU Free Documentation License
Group:      Applications/Productivity
BuildArch:  noarch
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root
URL:        http://granule.sourceforge.net

%description
granule-manual is the User's Manual for Granule index card program.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc


%changelog
* Tue Aug 10 2004 Vladislav Grinchenko <vlg@users.sourceforge.net> - manual-1
- Initial build.





